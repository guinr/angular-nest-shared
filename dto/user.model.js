"use strict";
exports.__esModule = true;
var User = /** @class */ (function () {
    function User(id, name, email, password) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
    }
    return User;
}());
exports.User = User;
